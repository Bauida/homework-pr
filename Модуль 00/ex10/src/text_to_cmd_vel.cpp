#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "string"

#include <sstream>
using namespace std;
int main(int argc, char **argv)
{
  ros::init(argc, argv, "text_to_cmd_vel");
  ros::NodeHandle nh;
  ros::Publisher cmd_vel_pub;

  cmd_vel_pub = nh.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1);

  geometry_msgs::Twist base_cmd;
  
  string cmd;
  while(nh.ok())
  {
    std::cin >> cmd;
    if(cmd == "turn_right")
    {
      base_cmd.angular.z = -0.5;
      base_cmd.linear.x = 0;
    }

    if(cmd == "turn_left")
    {
      base_cmd.angular.z = 0.5;
      base_cmd.linear.x = 0;
    }

    if(cmd == "move_forward")
    {
      base_cmd.angular.z = 0;
      base_cmd.linear.x = 1;
    }

    if(cmd == "move_backward")
    {
      base_cmd.angular.z = 0;
      base_cmd.linear.x = -1;
    }

   if(cmd ==  "exit")
   {
     break;
   }

    cmd_vel_pub.publish(base_cmd);
  }
  return 0;
}
