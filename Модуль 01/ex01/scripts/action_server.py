#! /usr/bin/env python 
import roslib
import rospy
import actionlib

from action_turtle_commands.msg import execute_turtle_commandsAction, execute_turtle_commandsResult, execute_turtle_commandsFeedback

from geometry_msgs.msg import Twist

class ActionServer:
    _feedback = execute_turtle_commandsFeedback()
    _result = execute_turtle_commandsResult()
    

    def __init__(self):
        self.server = actionlib.SimpleActionServer('action_turtle_command', execute_turtle_commandsAction, self.execute, False)
        self.server.start()

    def execute(self, goal):
        #r = rospy.Rate(1)
        self.distance = 0
        self.robot_move(goal.command, goal.s, goal.angle)


        for i in range(1, goal.s+1):
            
            self.distance += 1
            #r.sleep()
        self.server.set_succeeded(execute_turtle_commandsResult(self.distance))
    
    def robot_move(self, command, distanse_s, angle):
        #rospy.init_node('turtle_moving')
        pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        rate = rospy.Rate(10)  # 10
        turtle_velocity = Twist()
        s = 0
        turtle_velocity.linear.x = 0
        turtle_velocity.linear.y = 0
        turtle_velocity.linear.z = 0
        turtle_velocity.angular.x = 0
        turtle_velocity.angular.y = 0

        
        
        if command == "forward":
            while s < distanse_s:
                turtle_velocity.linear.x = 2.0
                turtle_velocity.linear.y = 0
                turtle_velocity.linear.z = 0
                turtle_velocity.angular.x = 0
                turtle_velocity.angular.y = 0
                turtle_velocity.angular.z = 0
                rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
                pub.publish(turtle_velocity)
                rate.sleep()
                s += 1
                self.server.publish_feedback(execute_turtle_commandsFeedback(s))
            turtle_velocity.linear.x = 0
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
            pub.publish(turtle_velocity)
            rate.sleep()

        elif command == "turn_left":
            turtle_velocity.angular.z = 30
            t0 = rospy.Time.now().to_sec()
            current_angle = 0

            while(current_angle < angle*2*3.14/360):
                pub.publish(turtle_velocity)
                t1 = rospy.Time.now().to_sec()
                current_angle = 30*(t1-t0)
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
                    
            turtle_velocity.angular.z = 0
            pub.publish(turtle_velocity)


        elif command == "turn_right":
            turtle_velocity.angular.z = -30
            t0 = rospy.Time.now().to_sec()
            current_angle = 0

            while(current_angle < angle*2*3.14/360):
                pub.publish(turtle_velocity)
                t1 = rospy.Time.now().to_sec()
                current_angle = 30*(t1-t0)
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
                    
            turtle_velocity.angular.z = 0
            pub.publish(turtle_velocity)    

if __name__ == '__main__':
    rospy.init_node('action_server')
    server =  ActionServer()
    print('Server run success!')
    rospy.spin()
    print('...')
